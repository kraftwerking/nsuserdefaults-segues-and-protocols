//
//  ViewController.m
//  NSUserDefaults, Segues and Protocols
//
//  Created by RJ Militante on 2/2/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import "ViewController.h"
#import "CreateAccountViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.usernameLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:USER_NAME];
    self.passwordLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:PASSWORD];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
