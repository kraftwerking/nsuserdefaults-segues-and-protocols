//
//  AppDelegate.h
//  NSUserDefaults, Segues and Protocols
//
//  Created by RJ Militante on 2/2/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

